use proc_macro::TokenStream;
use quote::quote;
use syn::__private::TokenStream2;
use syn::spanned::Spanned;
use syn::{
    parse_macro_input, Data, DataStruct, DeriveInput, Field, Fields, Index, Member, Meta,
    NestedMeta,
};

const REFLECT_ATTRIBUTE_NAME: &'static str = "reflect";

#[proc_macro_derive(Reflected, attributes(reflect))]
pub fn derive_reflected(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);

    match &ast.data {
        Data::Struct(s) => handle_struct(&ast, s),
        Data::Enum(_) => syn::Error::new(ast.span(), "Enums are not yet implemented")
            .into_compile_error()
            .into(),
        Data::Union(_) => syn::Error::new(ast.span(), "Unions are not supported")
            .into_compile_error()
            .into(),
    }
}

struct NamedStructField<'a> {
    pub field: &'a Field,
    pub index: usize,
    pub ignore: bool,
}

const REFLECT_IGNORE_NAME: &'static str = "ignore";

fn handle_field(index: usize, field: &Field) -> NamedStructField {
    let mut ignore = false;

    for attr in &field.attrs {
        if attr.path.is_ident(REFLECT_ATTRIBUTE_NAME) {
            let meta = attr.parse_meta().expect("Failed to parse meta");
            match meta {
                Meta::List(list) => {
                    for nested in &list.nested {
                        if let NestedMeta::Meta(m) = nested {
                            if let Meta::Path(p) = m {
                                if p.is_ident(REFLECT_IGNORE_NAME) {
                                    ignore = true;
                                }
                            }
                        }
                    }
                }
                _ => todo!(),
            }
        }
    }

    NamedStructField {
        index,
        field,
        ignore,
    }
}

fn handle_struct(input: &DeriveInput, ds: &DataStruct) -> TokenStream {
    //TODO: Collect generic data
    //TODO: collect fields
    //TODO: Collect trait info for partial_eq hash and clone and if those are available
    // call those traits rather than the custom ones

    let fields = match &ds.fields {
        Fields::Named(f) => f
            .named
            .iter()
            .enumerate()
            .map(|(index, field)| handle_field(index, field))
            .collect::<Vec<_>>(),
        _ => {
            return syn::Error::new(input.span(), "Only named structures are supported")
                .into_compile_error()
                .into();
        }
    };

    let (impl_generics, type_generics, where_clause) = input.generics.split_for_impl();

    let type_ident = &input.ident;

    let reflected_type = quote! {
        impl #impl_generics data_model_rs::Reflected for #type_ident #type_generics #where_clause{

            fn type_name(&self) ->&str {
                std::any::type_name::<Self>()
            }

            fn as_any(&self) ->&dyn core::any::Any {
                self
            }

            fn as_any_mut(&mut self) ->&mut dyn core::any::Any {
                self
            }

            fn as_reflected_kind(&self) -> data_model_rs::ReflectedKind<'_> {
                data_model_rs::ReflectedKind::Struct(self)
            }

            fn as_reflected_kind_mut(&mut self) -> data_model_rs::ReflectedKindMut<'_> {
                data_model_rs::ReflectedKindMut::Struct(self)
            }

            fn mutate(&mut self, other:&dyn Reflected) -> data_model_rs::MutateResult {
                data_model_rs::struct_mutate(self, other)
            }

            fn partial_eq(&self, other: &dyn Reflected) -> Option<bool> {
                data_model_rs::struct_partial_eq(self, other)
            }
        }
    };

    let structure_impl = generate_structure_impl(input, &fields);

    TokenStream::from(quote! {
        #reflected_type

        #structure_impl
    })
}

fn generate_structure_impl(ast: &DeriveInput, fields: &Vec<NamedStructField>) -> TokenStream2 {
    let member_names = fields
        .iter()
        .filter(|f| !f.ignore)
        .map(|f| -> String {
            f.field
                .ident
                .as_ref()
                .map(|v| v.to_string())
                .unwrap_or_else(|| f.index.to_string())
        })
        .collect::<Vec<_>>();

    let member_idents = fields
        .iter()
        .filter(|f| !f.ignore)
        .enumerate()
        .map(|(index, f)| {
            f.field
                .ident
                .as_ref()
                .map(|v| Member::Named(v.clone()))
                .unwrap_or_else(|| Member::Unnamed(Index::from(index)))
        })
        .collect::<Vec<_>>();

    let member_indices = fields
        .iter()
        .filter(|f| !f.ignore)
        .map(|f| -> usize { f.index })
        .collect::<Vec<_>>();

    let member_count = member_indices.len();

    let ident = &ast.ident;

    let (impl_generics, type_generics, where_clause) = ast.generics.split_for_impl();

    quote! {
        impl #impl_generics data_model_rs::Structure for #ident #type_generics #where_clause{
            fn member_len(&self) -> usize {
                #member_count
            }

            fn member(&self, name: &str) -> Option<&dyn data_model_rs::Reflected> {
                match name {
                    #(#member_names => Some(&self.#member_idents),)*
                    _=> None,
                }
            }

            fn member_mut(&mut self, name: &str) -> Option<&mut dyn data_model_rs::Reflected> {
                match name {
                    #(#member_names => Some(&mut self.#member_idents),)*
                    _=> None,
                }
            }

            fn member_at(&self, index: usize) -> Option<&dyn Reflected>{
                match index{
                    #(#member_indices => Some(&self.#member_idents),)*
                    _=> None,
                }
            }

            fn member_at_mut(&mut self, index: usize) -> Option<&mut dyn data_model_rs::Reflected> {
                match index{
                    #(#member_indices => Some(&mut self.#member_idents),)*
                    _=> None,
                }
            }

            fn member_name(&self, index: usize) -> Option<&str> {
                match index{
                    #(#member_indices => Some(#member_names),)*
                    _=> None,
                }
            }

            fn member_iter(&self) -> data_model_rs::MemberIter {
                data_model_rs::MemberIter::new(self)
            }
        }
    }
}
