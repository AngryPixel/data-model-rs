use crate::{MutateError, MutateResult, Reflected, ReflectedKind, ReflectedKindMut};
use std::any::Any;

macro_rules! impl_reflected_value {
    ($ttype:ty) => {
        impl Reflected for $ttype {
            fn type_name(&self) -> &str {
                std::any::type_name::<Self>()
            }

            fn as_any(&self) -> &dyn Any {
                self
            }

            fn as_any_mut(&mut self) -> &mut dyn Any {
                self
            }

            fn as_reflected_kind(&self) -> ReflectedKind<'_> {
                ReflectedKind::Value(self)
            }

            fn as_reflected_kind_mut(&mut self) -> ReflectedKindMut<'_> {
                ReflectedKindMut::Value(self)
            }

            fn mutate(&mut self, value: &dyn Reflected) -> MutateResult {
                let Some(v) = value.as_any().downcast_ref::<Self>() else {
                                                    return Err(MutateError::InvalidType)
                                                };
                *self = std::clone::Clone::clone(v);
                Ok(())
            }

            fn partial_eq(&self, other: &dyn Reflected) -> Option<bool> {
                let Some(v) = other.as_any().downcast_ref::<Self>() else {
                                                    return None;
                                                };

                Some(self == v)
            }
        }
    };
}

impl_reflected_value!(bool);
impl_reflected_value!(u8);
impl_reflected_value!(i8);
impl_reflected_value!(char);
impl_reflected_value!(i32);
impl_reflected_value!(u32);
impl_reflected_value!(u64);
impl_reflected_value!(i64);
impl_reflected_value!(isize);
impl_reflected_value!(usize);
impl_reflected_value!(f32);
impl_reflected_value!(f64);
impl_reflected_value!(String);
