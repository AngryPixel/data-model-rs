pub use data_model_rs_derive::Reflected;

mod impls;
mod structure;
pub use impls::*;
pub use structure::*;

use std::any::Any;

#[derive(Debug, PartialEq, Eq)]
pub enum MutateError {
    InvalidType,
    ReadOnly,
    MemberNotFound(String),
}

pub type MutateResult = Result<(), MutateError>;

pub enum ReflectedKind<'a> {
    Array,
    Enum,
    List,
    Map,
    Struct(&'a dyn Structure),
    Tuple,
    TupleStruct,
    Value(&'a dyn Reflected),
}

pub enum ReflectedKindMut<'a> {
    Array,
    Enum,
    List,
    Map,
    Struct(&'a dyn Structure),
    Tuple,
    TupleStruct,
    Value(&'a mut dyn Reflected),
}

pub trait Reflected: Any {
    fn type_name(&self) -> &str;

    fn as_any(&self) -> &dyn Any;

    fn as_any_mut(&mut self) -> &mut dyn Any;

    fn as_reflected_kind(&self) -> ReflectedKind<'_>;

    fn as_reflected_kind_mut(&mut self) -> ReflectedKindMut<'_>;

    fn mutate(&mut self, value: &dyn Reflected) -> MutateResult;

    fn partial_eq(&self, other: &dyn Reflected) -> Option<bool>;
}
