use crate::{MutateError, MutateResult, Reflected, ReflectedKind};

pub trait Structure: Reflected {
    fn member_len(&self) -> usize;

    fn member(&self, name: &str) -> Option<&dyn Reflected>;

    fn member_mut(&mut self, name: &str) -> Option<&mut dyn Reflected>;

    fn member_at(&self, index: usize) -> Option<&dyn Reflected>;

    fn member_at_mut(&mut self, index: usize) -> Option<&mut dyn Reflected>;

    fn member_name(&self, index: usize) -> Option<&str>;

    fn member_iter(&self) -> MemberIter;
}

pub struct MemberIter<'a> {
    s: &'a dyn Structure,
    index: usize,
}

impl<'a> MemberIter<'a> {
    pub fn new(s: &'a dyn Structure) -> Self {
        Self { s, index: 0 }
    }
}

impl<'a> Iterator for MemberIter<'a> {
    type Item = &'a dyn Reflected;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.s.member_len() {
            let r = self.s.member_at(self.index);
            self.index += 1;
            r
        } else {
            None
        }
    }
}

pub fn struct_partial_eq<S: Structure>(s: &S, other: &dyn Reflected) -> Option<bool> {
    let ReflectedKind::Struct(other) = other.as_reflected_kind() else {
        return Some(false);
    };

    if s.member_len() != other.member_len() {
        return Some(false);
    }

    for (i, member) in other.member_iter().enumerate() {
        let name = other.member_name(i).unwrap();
        if let Some(this_field) = s.member(name) {
            let result = this_field.partial_eq(member);
            if let failed @ (Some(false) | None) = result {
                return failed;
            }
        } else {
            return Some(false);
        }
    }

    Some(true)
}

pub fn struct_mutate<S: Structure>(s: &mut S, other: &dyn Reflected) -> MutateResult {
    let ReflectedKind::Struct(other) = other.as_reflected_kind() else {
        return Err(MutateError::InvalidType);
    };

    for (i, member) in other.member_iter().enumerate() {
        let name = other.member_name(i).unwrap();
        if let Some(struct_member) = s.member_mut(name) {
            struct_member.mutate(member)?;
        } else {
            return Err(MutateError::MemberNotFound(name.to_string()));
        }
    }

    Ok(())
}
