extern crate core;

use data_model_rs::Reflected;
use data_model_rs::*;

#[test]
fn test_derive_named_struct() {
    #[derive(Reflected, Debug)]
    struct Foo {
        a: i32,
        b: u32,
        c: u64,
        d: bool,
    }

    #[derive(Reflected, Debug)]
    struct Bar {
        a: i32,
        b: u32,
        c: u64,
        d: bool,
    }

    let mut foo = Foo {
        a: 1,
        b: 2,
        c: 3,
        d: false,
    };

    let bar = Bar {
        a: 10,
        b: 30,
        c: 3,
        d: true,
    };

    let ReflectedKindMut::Struct(s) = foo.as_reflected_kind_mut() else {
        panic!("Not a structure");
    };

    assert_eq!(s.member_len(), 4);
    assert_eq!(s.member_name(0), Some("a"));
    assert_eq!(s.member_name(1), Some("b"));
    assert_eq!(s.member_name(2), Some("c"));
    assert_eq!(s.member_name(3), Some("d"));
    assert_eq!(s.member_name(4), None);

    assert!(s
        .member("a")
        .expect("failed to get member")
        .partial_eq(&1_i32)
        .expect("Failed to compare"));
    assert!(s
        .member("b")
        .expect("failed to get member")
        .partial_eq(&2_u32)
        .expect("Failed to compare"));
    assert!(s
        .member("c")
        .expect("failed to get member")
        .partial_eq(&3_u64)
        .expect("Failed to compare"));
    assert!(s
        .member("d")
        .expect("failed to get member")
        .partial_eq(&false)
        .expect("Failed to compare"));

    assert_eq!(s.type_name(), "derive_tests::test_derive_named_struct::Foo");
    assert!(foo.mutate(&true).is_err());

    foo.mutate(&bar).expect("Failed to mutate foo");
    assert!(foo.partial_eq(&bar).expect("Failed to compare"));
}

#[test]
fn test_derive_ignore_field() {
    #[derive(Reflected, Debug)]
    struct Foo {
        a: i32,
        b: u32,
        c: u64,
        #[reflect(ignore)]
        d: bool,
    }

    let bar = Foo {
        a: 10,
        b: 30,
        c: 3,
        d: true,
    };

    assert_eq!(3, bar.member_len());
}

#[test]
fn test_derive_generic() {
    #[derive(Reflected, Debug)]
    struct Foo<T, Z: Clone + Reflected>
    where
        T: Clone + Reflected,
    {
        a: T,
        b: Z,
    }

    let foo = Foo::<i32, u32> { a: 10, b: 30 };

    assert_eq!(2, foo.member_len());
}
